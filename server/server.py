import socket
import select

HOST = "192.168.178.35"
PORT = 12345


def main():
    inputs = []
    outputs = []
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((HOST, PORT))
    server.listen()
    inputs.append(server)
    clients = []

    while True:
        print("Waiting for connections or data...")
        readable, writeable, exceptional = select.select(inputs, outputs, inputs)
        for handle in readable:
            if handle == server:
                user, address = server.accept()
                print(user, address)
                inputs.append(user)
                clients.append(user)
            else:
                data_recv = ""
                try:
                    data_recv = handle.recv(1024).decode("utf-8")
                except ConnectionError:
                    inputs.remove(handle)
                    clients.remove(handle)
                    handle.close()
                print(data_recv)
                for client in clients:
                    print(client)
                    client.send(data_recv.encode("utf-8"))




if __name__ == '__main__':
    main()
