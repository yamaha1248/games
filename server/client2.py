import socket
import sys
import select

HOST = "192.168.178.35"
PORT = 12345


def main():
    inputs = []
    outputs = []
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((HOST, PORT))
    inputs.append(client)
    inputs.append(sys.stdin)

    while True:
        readable, writeable, exceptional = select.select(inputs, outputs, inputs)
        for handle in readable:
            if handle == client:
                data_recv = client.recv(1024).decode("utf-8")
                print(data_recv)
            else:
                data_send = input()
                client.send(data_send.encode("utf-8"))


if __name__ == '__main__':
    main()
