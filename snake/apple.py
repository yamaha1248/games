import random
import pygame



class Apple():
    def __init__(self,sc, x_cells, y_cells, cell_size):
        self.x_cells = x_cells
        self.y_cells = y_cells
        self.cell_size = cell_size
        self.sc = sc
        self.x = random.randint(0, self.x_cells - 1)
        self.y = random.randint(0, self.y_cells - 1)
        self.color = (255, 0, 0)

    def get_xy(self):
        return (self.x, self.y)

    def set_color(self, color):
        self.color = color

    def create(self):
        self.x = random.randint(0, self.x_cells - 1)
        self.y = random.randint(0, self.y_cells - 1)


    def draw(self):
        pygame.draw.rect(self.sc, self.color, (self.x * self.cell_size, self.y * self.cell_size, self.cell_size, self.cell_size))

