import pygame
import random

FPS = 10
cell_size = 20
x_cells = 30
y_cells = 20

WHITE = (150, 150, 150)
GREEN = (0, 200, 0)
DARK_GREEN = (0, 100, 0)
BLACK = (0, 0, 0)
RED = (200, 0, 0)
YELLOW = (255, 255, 0)
ORANGE = (255, 127, 0)
BLUE = (0, 0, 255)
DARK_BLUE = (0, 0, 150)
color = RED


sc = pygame.display.set_mode((x_cells*cell_size,y_cells*cell_size))
clock = pygame.time.Clock()

eventKey1 = None
eventKey2 = None

def createApple():
    ax = random.randint(0, x_cells-1)
    ay = random.randint(0, y_cells-1)
    return (ax, ay)

apple = createApple()

def drawApple():
    pygame.draw.rect(sc, color, (apple[0] * cell_size, apple[1] * cell_size, cell_size, cell_size))

snake1 = [(1, 0), (0, 0)]
snake2 = [(x_cells-2, y_cells-1), (x_cells-1, y_cells-1)]

def drawSnake1():
    for i,xy in enumerate(snake1):
        if i == 0:
            pygame.draw.rect(sc, GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))

def drawSnake2():
    for i,xy in enumerate(snake2):
        if i == 0:
            pygame.draw.rect(sc, BLUE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_BLUE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))


def checkMove(xy):
    obstacles = snake1 + snake2
    if xy in obstacles:
        return False
    else:
        return True

def moveSnake1():
    global apple
    snakeMoved = False
    x0 = snake1[0][0]
    y0 = snake1[0][1]
    x1 = snake1[1][0]
    y1 = snake1[1][1]
    x = 0
    y = 0


    if eventKey1 == pygame.K_LEFT:
        if x0 > 0 and x0 <= x1:
            x = x0 - 1
            y = y0
            snakeMoved = True
    elif eventKey1 == pygame.K_RIGHT:
        if x0 < (x_cells - 1) and x0 >= x1:
            x = x0 + 1
            y = y0
            snakeMoved = True
    elif eventKey1 == pygame.K_UP:
        if y0 > 0 and y0 <= y1:
            x = x0
            y = y0 - 1
            snakeMoved = True
    elif eventKey1 == pygame.K_DOWN:
        if y0 < (y_cells - 1) and y0 >= y1:
            x = x0
            y = y0 + 1
            snakeMoved = True

    if snakeMoved and checkMove((x, y)):
        snake1.insert(0, (x, y))
        if apple == (x, y):
            apple = createApple()
            print("apple", apple)
        else:
            snake1.pop()

def moveSnake2():
    global apple
    snakeMoved = False
    x0 = snake2[0][0]
    y0 = snake2[0][1]
    x1 = snake2[1][0]
    y1 = snake2[1][1]
    x = 0
    y = 0



    if eventKey2 == pygame.K_z:              # влево
        if x0 > 0 and x0 <= x1:
            x = x0 - 1
            y = y0
            snakeMoved = True
    elif eventKey2 == pygame.K_c:                # вправо
        if x0 < (x_cells - 1) and x0 >= x1:
            x = x0 + 1
            y = y0
            snakeMoved = True
    elif eventKey2 == pygame.K_s:                # вверх
        if y0 > 0 and y0 <= y1:
            x = x0
            y = y0 - 1
            snakeMoved = True
    elif eventKey2 == pygame.K_x:                # вниз
        if y0 < (y_cells - 1) and y0 >= y1:
            x = x0
            y = y0 + 1
            snakeMoved = True

    if snakeMoved and checkMove((x, y)):
        snake2.insert(0, (x, y))
        if apple == (x, y):
            apple = createApple()
            print("apple", apple)
        else:
            snake2.pop()


def drawGrid():
    for x in range(x_cells):
        for y in range(y_cells):
            cell = pygame.Rect(x*cell_size, y*cell_size, cell_size, cell_size)
            pygame.draw.rect(sc, WHITE, cell, 1)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                eventKey1 = event.key
            if event.key == pygame.K_s or event.key == pygame.K_x or event.key == pygame.K_z or event.key == pygame.K_c:
                eventKey2 = event.key
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                eventKey1 = None
            if event.key == pygame.K_s or event.key == pygame.K_x or event.key == pygame.K_z or event.key == pygame.K_c:
                eventKey2 = None

    moveSnake1()
    moveSnake2()


    drawSnake1()
    drawSnake2()
    drawApple()
    drawGrid()
    pygame.display.update()
    clock.tick(FPS)