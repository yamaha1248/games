import pygame
import random

FPS = 10
cell_size = 20
x_cells = 30
y_cells = 20

WHITE = (150, 150, 150)
GREEN = (0, 200, 0)
DARK_GREEN = (0, 100, 0)
BLACK = (0, 0, 0)
RED = (200, 0, 0)
YELLOW = (255, 255, 0)
ORANGE = (255, 127, 0)
BLUE = (0, 0, 255)
DARK_BLUE = (0, 0, 150)
color = RED


sc = pygame.display.set_mode((x_cells*cell_size,y_cells*cell_size))
clock = pygame.time.Clock()

eventKey1 = None
eventKey2 = None

def createApple():
    ax = random.randint(0, x_cells-1)
    ay = random.randint(0, y_cells-1)
    return (ax, ay)

apple = createApple()

def drawApple():
    pygame.draw.rect(sc, color, (apple[0] * cell_size, apple[1] * cell_size, cell_size, cell_size))

snake1 = [(1, 0), (0, 0)]
snake2 = [(x_cells-2, y_cells-1), (x_cells-1, y_cells-1)]

def drawSnake1():
    for i,xy in enumerate(snake1):
        if i == 0:
            pygame.draw.rect(sc, GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))

def drawSnake2():
    for i,xy in enumerate(snake2):
        if i == 0:
            pygame.draw.rect(sc, BLUE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_BLUE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))

def checkMove(xy):
    obstacles = snake1 + snake2
    if xy in obstacles or xy[0] < 0 or xy[1] < 0 or xy[0] > x_cells-1 or xy[1] > y_cells-1:
        return False
    else:
        return True

def checkPoints():
    points1 = len(snake1) - 2
    points2 = len(snake2) - 2
    if points1 + points2 >= 5:
        return True
    else:
        return False

def gameOver(snake_num):

    if snake_num == 1:
        print('GAME OVER! победила синяя змея')
    elif snake_num == 2:
        print('GAME OVER! победила зелёная змея')
    else:
        points1 = len(snake1) - 2
        points2 = len(snake2) - 2

        if points1 > points2:
            print('GAME OVER! победила зелёная змея')
        elif points1 < points2:
            print('GAME OVER! победила синяя змея')
        else:
            print('GAME OVER! ничья')

        print('Очки зелёная:', points1, '| синяя:', points2)
    exit()

def moveSnake1():
    global apple
    x0 = snake1[0][0]
    y0 = snake1[0][1]
    x1 = snake1[1][0]
    y1 = snake1[1][1]
    x = 0
    y = 0

    if eventKey1 == pygame.K_LEFT and x0 <= x1:
        x = x0 - 1
        y = y0
    elif eventKey1 == pygame.K_RIGHT and x0 >= x1:
        x = x0 + 1
        y = y0
    elif eventKey1 == pygame.K_UP and y0 <= y1:
        x = x0
        y = y0 - 1
    elif eventKey1 == pygame.K_DOWN and y0 >= y1:
        x = x0
        y = y0 + 1
    else:
        if x0 < x1:
            x = x0-1
            y = y0
        elif x1 < x0:
            x = x0+1
            y = y0
        elif y0 < y1:
            y = y0-1
            x = x0
        elif y1 < y0:
            y = y0+1
            x = x0

    if not checkMove((x, y)):
        gameOver(1)

    snake1.insert(0, (x, y))
    if apple == (x, y):
        apple = createApple()
        print("apple", apple)
    else:
        snake1.pop()


def moveSnake2():
    global apple
    x0 = snake2[0][0]
    y0 = snake2[0][1]
    x1 = snake2[1][0]
    y1 = snake2[1][1]
    x = 0
    y = 0

    if eventKey2 == pygame.K_z and x0 <= x1:
        x = x0 - 1
        y = y0
    elif eventKey2 == pygame.K_c and x0 >= x1:
        x = x0 + 1
        y = y0
    elif eventKey2 == pygame.K_s and y0 <= y1:
        x = x0
        y = y0 - 1
    elif eventKey2 == pygame.K_x and y0 >= y1:
        x = x0
        y = y0 + 1
    else:
        if x0 < x1:
            x = x0 - 1
            y = y0
        elif x1 < x0:
            x = x0 + 1
            y = y0
        elif y0 < y1:
            y = y0 - 1
            x = x0
        elif y1 < y0:
            y = y0 + 1
            x = x0

    if not checkMove((x, y)):
        gameOver(2)

    snake2.insert(0, (x, y))
    if apple == (x, y):
        apple = createApple()
        print("apple", apple)
    else:
        snake2.pop()

def drawGrid():
    for x in range(x_cells):
        for y in range(y_cells):
            cell = pygame.Rect(x*cell_size, y*cell_size, cell_size, cell_size)
            pygame.draw.rect(sc, WHITE, cell, 1)

car_surf = pygame.image.load("C:\images\s.jpg")
car_rect = car_surf.get_rect()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                eventKey1 = event.key
            if event.key == pygame.K_s or event.key == pygame.K_x or event.key == pygame.K_z or event.key == pygame.K_c:
                eventKey2 = event.key
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                eventKey1 = None
            if event.key == pygame.K_s or event.key == pygame.K_x or event.key == pygame.K_z or event.key == pygame.K_c:
                eventKey2 = None

    moveSnake1()
    moveSnake2()

    if checkPoints():
        gameOver(0)

    drawSnake1()
    drawSnake2()
    drawApple()
    drawGrid()
    pygame.display.update()
    sc.blit(car_surf, car_rect)
    clock.tick(FPS)