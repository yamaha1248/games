import pygame
import random

FPS = 10
cell_size = 20
x_cells = 30
y_cells = 20

WHITE = (150, 150, 150)
GREEN = (0, 200, 0)
DARK_GREEN = (0, 100, 0)
BLACK = (0, 0, 0)
RED = (200, 0, 0)
YELLOW = (255, 255, 0)
DARK_YELLOW = (100, 100, 0)
ORANGE = (255, 127, 0)
BLUE = (0, 0, 255)
DARK_BLUE = (0, 0, 150)
color = RED

sc = pygame.display.set_mode((x_cells*cell_size,y_cells*cell_size))
clock = pygame.time.Clock()

def createApple():
    ax = random.randint(0, x_cells-1)
    ay = random.randint(0, y_cells-1)
    return (ax, ay)

apple = createApple()

def drawApple():
    pygame.draw.rect(sc, color, (apple[0] * cell_size, apple[1] * cell_size, cell_size, cell_size))

snake1 = [(1, 0), (0, 0)]
snake2 = [(x_cells-2, y_cells-1), (x_cells-1, y_cells-1)]
obstacles = snake1+snake2
def drawSnake1():
    for i,xy in enumerate(snake1):
        if i == 0:
            pygame.draw.rect(sc, GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))

def drawSnake2():
    for i,xy in enumerate(snake2):
        if i == 0:
            pygame.draw.rect(sc, YELLOW, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_YELLOW, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))


def moveSnake1(eventKey):
    global apple
    snakeMoved = False
    x0 = snake1[0][0]
    y0 = snake1[0][1]
    x1 = snake1[1][0]
    y1 = snake1[1][1]
    x = 0
    y = 0
    if eventKey == pygame.K_LEFT:
        if x0 > 0 and x0 <= x1:
            x = x0 - 1
            y = y0
            snakeMoved = True
    elif eventKey == pygame.K_RIGHT:
        if x0 < (x_cells - 1) and x0 >= x1:
            x = x0 + 1
            y = y0
            snakeMoved = True
    elif eventKey == pygame.K_UP:
        if y0 > 0 and y0 <= y1:
            x = x0
            y = y0 - 1
            snakeMoved = True
    elif eventKey == pygame.K_DOWN:
        if y0 < (y_cells - 1) and y0 >= y1:
            x = x0
            y = y0 + 1
            snakeMoved = True

    if snakeMoved:
        snake1.insert(0, (x, y))
        if apple == (x, y):
            apple = createApple()
            print("apple", apple)
        else:
            snake1.pop()

def moveSnake2(eventKey):
    global apple
    snakeMoved = False
    x0 = snake2[0][0]
    y0 = snake2[0][1]
    x1 = snake2[1][0]
    y1 = snake2[1][1]
    x = 0
    y = 0
    if eventKey == pygame.K_z:              # влево
        if x0 > 0 and x0 <= x1:
            x = x0 - 1
            y = y0
            snakeMoved = True
    elif eventKey == pygame.K_c:                # вправо
        if x0 < (x_cells - 1) and x0 >= x1:
            x = x0 + 1
            y = y0
            snakeMoved = True
    elif eventKey == pygame.K_s:                # вверх
        if y0 > 0 and y0 <= y1:
            x = x0
            y = y0 - 1
            snakeMoved = True
    elif eventKey == pygame.K_x:                # пвниз
        if y0 < (y_cells - 1) and y0 >= y1:
            x = x0
            y = y0 + 1
            snakeMoved = True

    if snakeMoved:
        snake2.insert(0, (x, y))
        if apple == (x, y):
            apple = createApple()
            print("apple", apple)
        else:
            snake2.pop()




def drawGrid():
    for x in range(x_cells):
        for y in range(y_cells):
            cell = pygame.Rect(x*cell_size, y*cell_size, cell_size, cell_size)
            pygame.draw.rect(sc, WHITE, cell, 1)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_y:
                color = YELLOW
            elif event.key == pygame.K_r:
                color = RED
            elif event.key == pygame.K_o:
                color = ORANGE

            moveSnake1(event.key)
            moveSnake2(event.key)

    sc.fill(BLACK)
    drawSnake2()
    drawSnake1()
    drawApple()
    drawGrid()
    pygame.display.update()
    clock.tick(FPS)
