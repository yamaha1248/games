import time

import pygame
import pygame_menu
import random
pygame.mixer.pre_init(44100, -16, 1, 512) # важно прописать до pygame.init()
pygame.init()

FPS = 1000
cell_size = 20
x_cells = 65
y_cells = 45
WHITE = (150, 150, 150)
GREEN = (0, 200, 0)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

ORANGE = (255, 127, 0)
BLUE = (0, 0, 255)
TURQUOISE = (0, 255, 255)
DARK_TURQUOISE = (0, 100, 100)
DARK_BLUE = (0, 0, 150)
DARK_GREEN = (0, 100, 0)
DARK_RED = (80, 0, 0)


sc = pygame.display.set_mode((x_cells*cell_size, (y_cells+1)*cell_size))
pygame.display.set_caption('ЗМЕЙКА')
pygame.display.set_icon(pygame.image.load("snake.jpg"))
clock = pygame.time.Clock()

# car_surf = pygame.image.load("C:\images\s.jpg")#C:\images\s.jpg
# car_rect = car_surf.get_rect()

# pygame.mixer.music.load("pygame-master\lesson 11.flowballs\sounds\\bird.mp3")
# pygame.mixer.music.play(-1)

apple_sound = pygame.mixer.Sound("pygame-master\\a.wav")
apple_sound.set_volume(0.1)
snake1 = []
snake2 = []
snake3 = []

apple = None

snake_dead_num = None

points_total = 5

def createSnake1():
    global snake1
    snake1 = [(1, 0), (0, 0)]

def createSnake2():
    global snake2
    snake2 = [(x_cells-2, y_cells-1), (x_cells-1, y_cells-1)]

def createSnake3():
    global snake3
    snake3 = [(x_cells-2, 0), (x_cells-1, 0)]

def drawApple():
    pygame.draw.rect(sc, RED, (apple[0] * cell_size, apple[1] * cell_size, cell_size, cell_size))

def drawSnake1():
    for i, xy in enumerate(snake1):
        if i == 0:
            pygame.draw.rect(sc, GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_GREEN, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))

def drawSnake2():
    for i, xy in enumerate(snake2):
        if i == 0:
            pygame.draw.rect(sc, BLUE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_BLUE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))

def drawSnake3():
    for i, xy in enumerate(snake3):
        if i == 0:
            pygame.draw.rect(sc, TURQUOISE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))
        else:
            pygame.draw.rect(sc, DARK_TURQUOISE, (xy[0] * cell_size, xy[1] * cell_size, cell_size, cell_size))

def checkMove(xy):
    obstacles = snake1 + snake2
    if xy in obstacles or xy[0] < 0 or xy[1] < 0 or xy[0] > (x_cells-2) or xy[1] > (y_cells-2):
        return False
    else:
        return True

def createApple():
    while True:
        ax = random.randint(0, x_cells-1)
        ay = random.randint(0, y_cells-1)
        if checkMove((ax, ay)):
            return (ax, ay)

def checkPoints():
    global points_total
    points = getPoints(snake1)+getPoints(snake2)+getPoints(snake3)
    if points >= points_total:
        return True
    else:
        return False

def gameOver(snake_num):
    global game_running, game_over
    game_running = False
    game_over = True

    global snake_dead_num
    snake_dead_num = snake_num

def drawGameOver():
    dead_render_header = None
    dead_render_body = None
    dead_render_winner = None
    dead_render_loser = None
    dead_render_score1 = None
    dead_render_score2 = None
    dead_render_computer = None

    points1 = len(snake1) - 2
    points2 = len(snake2) - 2
    points3 = len(snake3) - 2

    surf_game_over = pygame.image.load("C:pygame-master\images\k.jpg")
    game_over_rect = surf_game_over.get_rect()

    font_render_header = pygame.font.SysFont("Segoe UI", cell_size+20)
    font_render_body = pygame.font.SysFont("Segoe UI", cell_size)
    font_render_winner = pygame.font.SysFont("Segoe UI", cell_size)
    font_render_loser = pygame.font.SysFont("Segoe UI", cell_size)
    font_render_score1 = pygame.font.SysFont("Segoe UI", cell_size)
    font_render_score2 = pygame.font.SysFont("Segoe UI", cell_size)
    font_render_computer = pygame.font.SysFont("Segoe UI", cell_size)


    dead_render_header = font_render_header.render("ИГРА ЗАКОНЧЕНА!", 1, RED)
    if len(snake1) > 0 and len(snake2) > 0:
        if snake_dead_num == 1:
            dead_render_body = font_render_body.render("Победила синяя змея", 1, RED)
            dead_render_winner = font_render_winner.render("синяя", 1, RED)
            dead_render_loser = font_render_loser.render("зелёная", 1, RED)
            dead_render_score1 = font_render_score1.render(f"счет {points2}", 1, RED)
            dead_render_score2 = font_render_score2.render(f"счет {points1}", 1, RED)
        elif snake_dead_num == 2:
            dead_render_body = font_render_body.render("Победила зелёная змея", 1, RED)
            dead_render_winner = font_render_winner.render("зелёная", 1, RED)
            dead_render_loser = font_render_loser.render("синяя", 1, RED)
            dead_render_score1 = font_render_score1.render(f"счет {points1}", 1, RED)
            dead_render_score2 = font_render_score2.render(f"счет {points2}", 1, RED)
        else:
            if points1>points2:
                dead_render_body = font_render_body.render("Победила зелёная змея", 1, RED)
                dead_render_winner = font_render_winner.render("зелёная", 1, RED)
                dead_render_loser = font_render_loser.render("синяя", 1, RED)
                dead_render_score1 = font_render_score1.render(f"счет {points1}", 1, RED)
                dead_render_score2 = font_render_score2.render(f"счет {points2}", 1, RED)
            elif points1<points2:
                dead_render_body = font_render_body.render("Победила синяя змея", 1, RED)
                dead_render_winner = font_render_winner.render("синяя", 1, RED)
                dead_render_loser = font_render_loser.render("зелёная", 1, RED)
                dead_render_score1 = font_render_score1.render(f"счет {points2}", 1, RED)
                dead_render_score2 = font_render_score2.render(f"счет {points1}", 1, RED)
            else:
                dead_render_body = font_render_body.render("ничья", 1, RED)
                dead_render_winner = font_render_winner.render("зелёная", 1, RED)
                dead_render_loser = font_render_loser.render("синяя", 1, RED)
                dead_render_score1 = font_render_score1.render(f"счет {points1}", 1, RED)
                dead_render_score2 = font_render_score2.render(f"счет {points2}", 1, RED)
    elif len(snake1) > 0 and len(snake3) > 0:
        if snake_dead_num == 1:
            dead_render_body = font_render_body.render("Победила голубая змея", 1, RED)
            dead_render_winner = font_render_winner.render("голубая", 1, RED)
            dead_render_loser = font_render_loser.render("зелёная", 1, RED)
            dead_render_score1 = font_render_score1.render(f"счет {points3}", 1, RED)
            dead_render_score2 = font_render_score2.render(f"счет {points1}", 1, RED)
        elif snake_dead_num == 3:
            dead_render_body = font_render_body.render("Победила зелёная змея", 1, RED)
            dead_render_winner = font_render_winner.render("зелёная", 1, RED)
            dead_render_loser = font_render_loser.render("голубая", 1, RED)
            dead_render_score1 = font_render_score1.render(f"счет {points1}", 1, RED)
            dead_render_score2 = font_render_score2.render(f"счет {points3}", 1, RED)
        else:
            if points1 > points3:
                dead_render_body = font_render_body.render("Победила зелёная змея", 1, RED)
                dead_render_winner = font_render_winner.render("зелёная", 1, RED)
                dead_render_loser = font_render_loser.render("голубая", 1, RED)
                dead_render_score1 = font_render_score1.render(f"счет {points1}", 1, RED)
                dead_render_score2 = font_render_score2.render(f"счет {points3}", 1, RED)
            elif points1 < points3:
                dead_render_body = font_render_body.render("Победила синяя змея", 1, RED)
                dead_render_winner = font_render_winner.render("голубая", 1, RED)
                dead_render_loser = font_render_loser.render("зелёная", 1, RED)
                dead_render_score1 = font_render_score1.render(f"счет {points3}", 1, RED)
                dead_render_score2 = font_render_score2.render(f"счет {points1}", 1, RED)
            else:
                dead_render_body = font_render_body.render("ничья", 1, RED)
                dead_render_winner = font_render_winner.render("зелёная", 1, RED)
                dead_render_loser = font_render_loser.render("голубая", 1, RED)
                dead_render_score1 = font_render_score1.render(f"счет {points1}", 1, RED)
                dead_render_score2 = font_render_score2.render(f"счет {points3}", 1, RED)


    elif len(snake2) > 0:
        if snake_dead_num == 2:
            dead_render_body = font_render_body.render("компьютер проиграл", 1, RED)
            dead_render_computer = font_render_computer.render(f"счет {points2}", 1, RED)
        else:
            dead_render_body = font_render_body.render("компьютер выиграл", 1, RED)
            dead_render_computer = font_render_computer.render(f"счет {points2}", 1, RED)
    elif len(snake1) > 0:
        if snake_dead_num == 1:
            dead_render_body = font_render_body.render("вы проиграли", 1, RED)
            dead_render_computer = font_render_computer.render(f"счет {points1}", 1, RED)
        else:
            dead_render_body = font_render_body.render("вы выиграли", 1, RED)
            dead_render_computer = font_render_computer.render(f"счет {points1}", 1, RED)

    sc.blit(surf_game_over, game_over_rect)
    sc.blit(dead_render_header, ((x_cells*cell_size)//3, (y_cells*cell_size)//10))
    if len(snake1) > 0 and len(snake2) > 0 or len(snake1) > 0 and len(snake3) > 0:
        sc.blit(dead_render_body, ((x_cells*cell_size)//2.7, (y_cells*cell_size)//5))
        sc.blit(dead_render_winner, ((x_cells*cell_size)//2.5, (y_cells*cell_size)//3.5))
        sc.blit(dead_render_loser, ((x_cells*cell_size)//1.7, (y_cells*cell_size)//3.5))
        sc.blit(dead_render_score1, ((x_cells*cell_size)//2.4, (y_cells*cell_size)//2.5))
        sc.blit(dead_render_score2, ((x_cells*cell_size)//1.7, (y_cells*cell_size)//2.5))
    elif len(snake2) > 0:
        sc.blit(dead_render_body, ((x_cells * cell_size) // 2.5, (y_cells * cell_size) // 5))
        sc.blit(dead_render_computer, ((x_cells*cell_size)//2, (y_cells*cell_size)//3.5))
    elif len(snake1) > 0:
        sc.blit(dead_render_body, ((x_cells*cell_size)//2.5, (y_cells*cell_size)//5))
        sc.blit(dead_render_computer, ((x_cells*cell_size)//2.3, (y_cells*cell_size)//3.5))

def getPoints(snake):
    if len(snake) == 0:
        return 0
    else:
        return len(snake) - 2

def getScoreRender():
    score_render = None

    points1 = getPoints(snake1)
    points2 = getPoints(snake2)
    points3 = getPoints(snake3)

    font = pygame.font.SysFont("Segoe UI", cell_size)

    if len(snake1) > 0 and len(snake2) > 0:
        score_render = font.render(f"Счет: зелёная {points1}  | синяя {points2}", 1, RED)
    elif len(snake1) > 0 and len(snake3) > 0:
        score_render = font.render(f"Счет: зелёная {points1}  |  голубая {points3}", 1, RED)
    elif len(snake2) > 0:
        score_render = font.render(f"Счет: {points2}", 1, RED)
    elif len(snake1) > 0:
        score_render = font.render(f"Счет:  {points1}", 1, RED)
    else:
        score_render = font.render("", 1, RED)
    return score_render

def moveSnake1():
    if len(snake1) < 2:
        return

    global apple
    x0 = snake1[0][0]
    y0 = snake1[0][1]
    x1 = snake1[1][0]
    y1 = snake1[1][1]
    x = 0
    y = 0

    if eventKey1 == pygame.K_LEFT and x0 <= x1:
        x = x0 - 1
        y = y0
    elif eventKey1 == pygame.K_RIGHT and x0 >= x1:
        x = x0 + 1
        y = y0
    elif eventKey1 == pygame.K_UP and y0 <= y1:
        x = x0
        y = y0 - 1
    elif eventKey1 == pygame.K_DOWN and y0 >= y1:
        x = x0
        y = y0 + 1
    else:
        if x0 < x1:
            x = x0-1
            y = y0
        elif x1 < x0:
            x = x0+1
            y = y0
        elif y0 < y1:
            y = y0-1
            x = x0
        elif y1 < y0:
            y = y0+1
            x = x0

    if not checkMove((x, y)):
        gameOver(1)

    snake1.insert(0, (x, y))
    if apple == (x, y):
        apple_sound.play()
        apple = createApple()
    else:
        snake1.pop()

def moveSnake2():
    if len(snake2) < 2:
        return

    moves = []
    moves_checked = []
    moves_distance = []
    moves_xy_min = []
    move_xy = None


    global apple
    x0 = snake2[0][0]
    y0 = snake2[0][1]
    moves.append((x0-1, y0))
    moves.append((x0  , y0+1))
    moves.append((x0+1, y0))
    moves.append((x0, y0-1))

    # получаем координаты куда может ходить змейка
    for xy in moves:
        if checkMove(xy):
            moves_checked.append(xy)

    # если ходить некуда, то змейка проиграла
    if len(moves_checked) == 0:
        gameOver(2)
        return
    # находим для какого хода минимальное расстояние
    for xy in moves_checked:
        moves_distance.append(getDistance(xy, apple))
    min_distance = min(moves_distance)

    for i, d in enumerate(moves_distance):
        if d == min_distance:
            moves_xy_min.append(moves_checked[i])
    move_xy = random.choice(moves_xy_min)

    snake2.insert(0, move_xy)
    if apple == move_xy:
        apple = createApple()
        apple_sound.play()
    else:
        snake2.pop()

def moveSnake3():
    if len(snake3) < 2:
        return

    global apple
    x0 = snake3[0][0]
    y0 = snake3[0][1]
    x1 = snake3[1][0]
    y1 = snake3[1][1]
    x = 0
    y = 0

    if eventKey3 == pygame.K_z and x0 <= x1:
        x = x0 - 1
        y = y0
    elif eventKey3 == pygame.K_c and x0 >= x1:
        x = x0 + 1
        y = y0
    elif eventKey3 == pygame.K_s and y0 <= y1:
        x = x0
        y = y0 - 1
    elif eventKey3 == pygame.K_x and y0 >= y1:
        x = x0
        y = y0 + 1
    else:
        if x0 < x1:
            x = x0-1
            y = y0
        elif x1 < x0:
            x = x0+1
            y = y0
        elif y0 < y1:
            y = y0-1
            x = x0
        elif y1 < y0:
            y = y0+1
            x = x0

    if not checkMove((x, y)):
        gameOver(3)

    snake3.insert(0, (x, y))
    if apple == (x, y):
        apple = createApple()
        apple_sound.play()
    else:
        snake3.pop()

def getDistance(xy1, xy2):
    return abs(xy1[1]-xy2[1])+abs(xy1[0]-xy2[0])

def drawGrid():
    for x in range(x_cells):
        for y in range(y_cells):
            cell = pygame.Rect(x*cell_size, y*cell_size, cell_size, cell_size)
            pygame.draw.rect(sc, WHITE, cell, 1)

def startTheGamePlayerAndComputer():
    global game_running, game_over
    game_running = True
    game_over = False

    global snake1, snake2, snake3
    snake1 = []
    snake2 = []
    snake3 = []
    createSnake1()
    createSnake2()

    global apple
    apple = createApple()

def startTheGamePlayerAndPlayer():
    global game_running, game_over
    game_running = True
    game_over = False

    global snake1, snake2, snake3
    snake1 = []
    snake2 = []
    snake3 = []
    createSnake1()
    createSnake3()

    global apple
    apple = createApple()

def startTheGamePlayer():
    global game_running, game_over
    game_running = True
    game_over = False

    global snake1, snake2, snake3
    snake1 = []
    snake2 = []
    snake3 = []
    createSnake1()

    global apple
    apple = createApple()

game_running = False
game_over = False
def startTheGameComputer():
    global game_running, game_over
    game_running = True
    game_over = False

    global snake1, snake2, snake3
    snake1 = []
    snake2 = []
    snake3 = []
    createSnake2()

    global apple
    apple = createApple()

def stopTheGame():
    global game_running, game_over
    game_running = False
    game_over = False
    global snake1, snake2, snake3
    snake1 = []
    snake2 = []
    snake3 = []

def setTotalPoints(value, points):
    global points_total
    points_total = points

def playGame(events):
    global eventKey1
    global eventKey3
    for event in events:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                eventKey1 = event.key
            if event.key == pygame.K_s or event.key == pygame.K_x or event.key == pygame.K_z or event.key == pygame.K_c:
                eventKey3 = event.key
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                eventKey1 = None
            if event.key == pygame.K_s or event.key == pygame.K_x or event.key == pygame.K_z or event.key == pygame.K_c:
                eventKey3 = None

    moveSnake1()
    moveSnake2()
    moveSnake3()

    if checkPoints():
        gameOver(0)

    sc.fill(BLACK)
    # sc.blit(car_surf, car_rect)

    drawSnake1()
    drawSnake2()
    drawSnake3()
    drawApple()
    drawGrid()
    sc.blit(getScoreRender(), (y_cells * cell_size-100, y_cells * cell_size - cell_size // 4))

menu = pygame_menu.Menu('', x_cells*cell_size, y_cells*cell_size,theme=pygame_menu.themes.THEME_BLUE)
menu.add.selector('количество очков :', [('5', 5), ('20', 20), ('50', 50), ('100', 100)], onchange=setTotalPoints)
menu.add.button('игрок + компьютер', startTheGamePlayerAndComputer)
menu.add.button('игрок + игрок', startTheGamePlayerAndPlayer)
menu.add.button('компьютер', startTheGameComputer)
menu.add.button('игрок', startTheGamePlayer)
menu.add.button('выход', pygame_menu.events.EXIT)

eventKey1 = None
eventKey3 = None
while True:
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                stopTheGame()

    if game_running:
        playGame(events)
    elif game_over:
        drawGameOver()
        events = None
    else:
        menu.update(events)
        menu.draw(sc)
        events = None

    pygame.display.update()
    clock.tick(FPS)