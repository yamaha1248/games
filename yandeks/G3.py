def main(N, lst):
    M = 0
    new_lst = []

    for i in range(N):
        if lst[i] not in new_lst:
            new_lst.append(lst[i])



    # lst = list(set(lst))

    for i in range(len(new_lst)):
        if new_lst[i][0] >= 0 and new_lst[i][1] >= 0:
            if (new_lst[i][0] + new_lst[i][1] + 1) == N:
                M += 1

    return M


if __name__ == '__main__':
    N = int(input())
    lst = []
    for _ in range(N):
        lst.append(tuple(map(int, input().split())))
    res = main(N, lst)
    print(res)
