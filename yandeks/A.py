n = int(input())
dict_ = {}

for _ in range(n):
    k, v = input().split()
    dict_[k] = v

synonim = input()

if synonim in dict_.keys():
    print(dict_.get(synonim))
else:
    dict_keys = list(dict_.keys())
    dict_values = list(dict_.values())
    myValue = synonim
    val_index = dict_values.index(myValue)
    myKey = dict_keys[val_index]
    print(myKey)
