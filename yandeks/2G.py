def my_max(lst):
    max_ = None
    for i in lst:
        if max_ == None:
            max_ = i
        elif max_ < i:
            max_ = i
    return max_

def my_min(lst):
    min_ = None
    for i in lst:
        if min_ == None:
            min_ = i
        elif min_ > i:
            min_ = i
    return min_

def main(lst):
    if len(lst) == 2:
        if lst[0] >= lst[1]:
            return lst[1], lst[0]
        else:
            return lst[0], lst[1]
    max1 = my_max(lst)
    min1 = my_min(lst)
    lst.remove(max1)
    lst.remove(min1)
    max2 = my_max(lst)
    min2 = my_min(lst)
    max_ = max1*max2
    min_ = min1*min2
    if max_ > min_:
        if max1 >= max2:
            return max2, max1
        else:
            return max1, max2
    else:
        if min1 >= min2:
            return min2, min1
        else:
            return min1, min2

if __name__ == '__main__':
    i = input().split()
    res = main([int(_) for _ in i])
    print(*res)