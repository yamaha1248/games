def getDetailsNum(N, K, M):
    d_num = 0
    while N >= K and N >= M and K >= M:
        z_num = N // K
        z_mod = N % K
        d_z_num = K // M
        d_z_mod = K % M
        d_num += z_num * d_z_num
        d_mod = z_mod + d_z_mod * z_num
        N = d_mod

    return d_num


if __name__ == '__main__':
    N, K, M = map(int, input().split())
    result = getDetailsNum(N, K, M)
    print(result)

# def getDetailsNum(N, K, M):
#     d_num = 0
#     while N >= K and N >= M and K>=M:
#         z_num = N // K
#         z_mod = N % K
#         d_z_num = K // M
#         d_z_mod = K % M
#         d_num += z_num * d_z_num
#         d_mod = z_mod + d_z_mod * z_num
#         N = d_mod
#
#     return d_num
#
#
# if __name__ == '__main__':
#     # N, K, M = map(int, input().split())
#     # getDetailsNum(N, K, M)
#     for n in range(1, 201):
#         for k in range(1, 201):
#             for m in range(1, 201):
#                 print(n, k, m)
#                 getDetailsNum(n, k, m)
