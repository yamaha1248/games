N, M = map(int, input().split())
lst1 = []
lst2 = []
i = 0
for i in range(N):
    lst1.append(int(input()))

for j in range(M):
    lst2.append(int(input()))


def sort(a):
    n = 1
    while n < len(a):
        for i in range(len(a) - n):
            if a[i] > a[i + 1]:
                a[i], a[i + 1] = a[i + 1], a[i]
        n += 1
    return a


set_list1 = list(set(lst1) & set(lst2))
set_list2 = list(set(lst1) - set(lst2))
set_list3 = list(set(lst2) - set(lst1))

print(len(set_list1))
print(*sorted(set_list1))

print(len(set_list2))
print(*sorted(set_list2))

print(len(set_list3))
print(*sorted(set_list3))
