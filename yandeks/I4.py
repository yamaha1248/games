def is_one_accent(world):
    accent_counter = 0
    for letter in world:
        if letter.isupper():
            accent_counter += 1
    if accent_counter == 1:
        return True
    else:
        return False


def main(lst, text_lst):
    mistake_counter = 0
    for w in text_lst:
        if w in lst:
            continue
        elif is_one_accent(w):
            continue
        else:
            mistake_counter += 1
    return mistake_counter


if __name__ == '__main__':
    N = int(input())
    words_dict = []
    for w in range(N):
        words_dict.append(input())
    text_lst = input().split()

    res = main(words_dict, text_lst)
    print(res)
