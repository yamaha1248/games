i = input().split()

lst = [int(i) for i in i]


last = None
for i in lst:
    if last == None:
        last = i
    elif last < i:
        last = i
    else:
        print("NO")
        break

else:
    print("YES")