# N = input()
# lst_N = [int(i) for i in input().split()]
#
# M = input()
# lst_M = [int(i) for i in input().split()]
#
# d = {}
# min_d = None
# for i in lst_N:
#
#     if i < lst_M[0]:
#         d[abs(i-lst_M[0])] = i, lst_M[0]
#         continue
#     elif i > lst_M[-1]:
#         d[abs(i - lst_M[-1])] = i, lst_M[-1]
#         continue
#
#     last_abs = None
#     last_ij = ()
#     for j in lst_M:
#         if j < lst_N[0]:
#             d[abs(lst_N[0] - j)] = lst_N[0], j
#             continue
#         elif j > lst_N[-1]:
#             d[abs(lst_N[-1] - j)] = lst_N[-1], j
#             continue
#
#         abs_ij = abs(i - j)
#         if last_abs == None:
#             last_abs = abs_ij
#             last_ij = i, j
#             d[abs_ij] = i, j
#         elif abs_ij < last_abs:
#             d[abs_ij] = i, j
#             last_abs = abs_ij
#             last_ij = i, j
#         else:
#             break
# print(*d[min(d.keys())])

N = input()
lst_N = [int(i) for i in input().split()]

M = input()
lst_M = [int(i) for i in input().split()]

min_abs = None
min_NM = ()
start_j = 0
for val_i in lst_N:
    for j in range(start_j, len(lst_M)):
        val_j = lst_M[j]
        abs_NM = abs(val_i - val_j)
        if min_abs is None:
            min_abs = abs_NM
            min_NM = val_i, val_j
        elif abs_NM < min_abs:
            min_abs = abs_NM
            min_NM = val_i, val_j
        if val_j > val_i:
            start_j = j
            break

print(*min_NM)
