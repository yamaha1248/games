def main(lst):
    lst_len = len(lst)
    lst_max = max(lst)
    lst_max_idx = lst.index(lst_max)

    distances = []

    for i in range(lst_len - 1):
        if lst[i] % 5 == 0 and lst[i] % 2 != 0:  # последняя цифра 5
            if i > lst_max_idx:
                if lst[i] > lst[i + 1]:
                    distances.append(lst[i])



    if len(distances) == 0:
        return 0
    elif max(distances) == lst_max:
        return 1
    else:
        k = 1
        distances_max = max(distances)
        for val in lst:
            if val > distances_max:
                k += 1
        return k


if __name__ == '__main__':
    _ = input()
    input_lst = [int(i) for i in input().split()]
    res = main(input_lst)
    print(res)
