import pprint
N = int(input())
lst = []
for _ in range(N):
    w, h = map(int, input().split())
    lst.append([w, h])
d = {"1": [], "2": [], "3": [], "4": [], "5": [], "6": [], "7": [], "8": [], "9": []}
for i in lst:
    if str(i[0]) not in d.keys():
        d[str(i[0])] = []
    d.get(str(i[0])).append(i)

for k, v in d.items():
    if len(v) > 1:
        d[k] = max(v)

h = 0
for i in d.values():
    if str(i).count("[") > 1:
        i = i[0]
    if len(i) > 0:
        h += i[1]

pprint.pprint(d)
print(h)
